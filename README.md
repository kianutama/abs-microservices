# NON TECHNICAL

* Microservice berperan sebagai penyedia API,
nanti fronend akan nembak API ini untuk mendapatkan data.

* Directory structure:
  * framework: 
    * resource: menerima routing aplikasi, kemudian mengarahkan ke lapisan service terkait.
    * service: implementasi business logic, pemanggilan ke lapisan entity model terkait fitur ataupun model lain, kemudian melakukan pengolahan data yang diterima dari lapisan entity.
    * entity: implementasi fungsi yang sering digunakan untuk mengakses ke basis data, seperti save, update, delete, dan list.
    * framework: konfigurasi aplikasi seperti routing, utility, product config dan kebutuhan lainnya.
    * delta: implementasi delta module dari aplikasi, dipisah sesuai dengan lapisan masing-masing.
  * middleware: segala hal yg berkaitan dengan auth (facebook auth, google auth)

### Flow 
![Microservice Message Flow](docs/microservice-arch.png)

![ABS Microservices Framework](docs/NewFramework.png)

### Development Step
1. Buat model ABS untuk fitur yang ingin dibuat.
2. Buat lapisan entity dan objek filter.
    1. Fungsi standar pada satu lapisan entity adalah:
        - Save Model
        - Update Model
        - Delete Model
        - List Model
    2. Membuat objek filter sesuai dengan atribut yang ingin di filter.
    3. Membuat fungsi list yang lebih fleksibel jika dibutuhkan, menggunakan objek filter yang sebelumnya telah dibuat.
3. Buat lapisan service, implementasi business logic sesuai kebutuhan. Pada lapisan ini, bisa melakukan pemanggilan ke lapisan entity model tersebut ataupun model lain. Pada lapisan ini, pemanggilan lapisan service lain juga dapat dilakukan.
4. Buat lapisan resource, pada resource 1 endpoint akan direpresentasikan oleh 1 fungsi pada resource yang akan memanggil fungsi pada lapisan service.
5. Buat routing, routing dikategorikan perfitur kemudian buat fungsi untuk setiap case yang ada pada fitur/model tersebut.

# TECHNICAL
Untuk membuat atau membangun suatu aplikasi menggunakan abs-microservices diperlukan untuk melakukan beberapa cara. 

Secara umum, pada repository ini, terdapat dua folder utama yang memiliki peranan berbeda. Folder pertama yang berada pada directory "[root]/framework" merupakan aplikasi utama dimana kita mengimplementasikan kode dan mendefinisikan suatu produk dengan fitur-fitur tertentu. Kemudian, folder kedua yang berada pada directory "[root]/middleware" merupakan sebuah tools untuk menghubungkan antara dunia luar (request http) dan program utama.

## Middleware
Standarnya, middleware sudah di-build dan diletakkan pada folder "framework/lib" dapat digunakan tanpa perlu melakukan apa-apa. Namun, apabila terdapat perubahan pada implementasi middleware maka diperlukan untuk melakukan beberapa tahapan berikut.

### Build Middleware
1. Menggunakan aplikasi terminal atau command line, pergi ke directory "[root]/middleware".
2. Jalankan perintah `ant` pada folder "/middleware" kemudian tunggu beberapa saat
3. Hasil dari perintah tersebut dapat dilihat pada directory "[root]/middleware/dist/middleware.jar"

### Using Middleware
Hasil build sebelumnya, dipindahkan ke directory "[root]/framework/lib". Seharusnya sudah ada file "middleware.jar" sehingga saat memindahkan, ganti file yang lama dengan file yang baru. Hal ini dilakukan agar saat kemudian produk aplikasi akan dibuild, perubahan implementasi middleware akan digunakan.

## Application Product
Produk aplikasi yang akan dibuat juga harus melakukan beberapa tahapan, agar aplikasi yang dihasilkan akan mengimplementasikan core module dan delta module yang sesuai dengan produk yang akan dibuat.

### Defining Product

Pertama, perlu diperhatikan apabila terdapat fitur yang baru dikembangkan, fitur tersebut harus didefinisikan pada product line pada file "[root]/framework/src/abs/framework/ProductConfig.abs". Pada baris **productline**, tidak diperlukan untuk menambahkan **productline** baru, namun pada bagian **features** perlu ditambahkan nama fitur yang baru saja dikembangkan.

Contoh:
> productline COS;  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;features Activity, FinancialReport, Income, Expense, Report, Manual, Automatic,  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;..., ..., ..., NewFeature;

Untuk membuat sebuah produk baru, maka produk perlu didefinisikan pada file "[root]/framework/src/abs/framework/Products.abs". Pada file tersebut, nama produk harus didefinisikan beserta fitur-fitur apa saja yang ada pada produk tersebut.

Contoh: 
> product AISCONewProduct (Activity,Income,Expense,PSAK45,AboutUs,Contact,Home,NewFeature);

### Build Product
1. Menggunakan aplikasi terminal atau command line, pergi ke directory "[root]/framework".
2. Buka file "build.sh" kemudian ubah nilai dari parameter `-Dabsproduct` menjadi nama produk yang kita ingin buat.  
Contoh:  
`ant -buildfile "${DIR}/build.xml" -Dabsproduct=AISCONewProduct abs.deploy`
3. Jalankan perintah 
   - Windows : `bash build.sh`
   - Linux & MacOS: `./build.sh`
4. Hasil dari perintah tersebut dapat dilihat pada directory "[root]/framework/[nilai dari parameter `-Dabsproduct`].jar"

### Run Product
1. Menggunakan aplikasi terminal atau command line, pergi ke directory "[root]/framework".
2. Buka file "run.sh" kemudian ubah nama file .jar yang ingin dijalankan.  
Contoh:  
`java -jar "${DIR}/AISCONewProduct.jar"`
3. Jalankan perintah 
   - Windows : `bash run.sh`
   - Linux & MacOS: `./run.sh`
4. Aplikasi akan berjalan pada port 8081, sehingga dapat diakses pada http://localhost:8081
5. Akses endpoint melalui routing yang telah didefinisikan pada file "[root]/framework/src/abs/framework/Route.abs"

## Run Application with Frontend
Untuk menjalankan aplikasi yang sudah dibuat sebelumnya dilakukan menggunakan project yang ada pada repository:  
[abs-ifml-deployment-cli](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/abs-ifml-deployment-cli) 

Untuk step-by-step penggunaannya dapat dilihat pada README.md repository terkait.

## Other

AbsDbOrm.jar => dihasilkan dari AbsDbOrm.java (ada di absmvc)
  - untuk dapetinnya coba clone absmvc, build disitu, copy lib/AbsDbOrm.jar nya


# Pending Branch
Pengembangan yang dilakukan pada repository ini sudah digabungkan(merge) ke branch master. Namun, terdapat branch yang belum digabungkan pada branch master karena adanya isu yang harus diselesaikan terlebih dahulu.

## Atribut Model yang Sesuai
Branch: [new-model-attribute](https://gitlab.com/kianutama/abs-microservices/-/tree/new-model-attribute)

Pada branch ini, model yang memiliki atribut-atribut yang seharusnya menggunakan tipe data model lain benar diimplementasikan seperti itu. Pada branch lain belum diimplementasikan cara seperti ini, karena ABS DB ORM masih belum dapat memproses atribut dengan tipe data model lain(tidak primitif). Namun, karena adanya permasalahan tersebut, branch ini belum di merge ke branch master. **(Merge apabila isu ABS ORM sudah terselesaikan)**
